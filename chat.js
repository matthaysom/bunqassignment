const IMAGE_DIAMETER = 80;

let userId;
let currentConversationId;
let currentMessageId;

const apiUrl = `http://assignment.bunq.com`;
function getAllUsers() {return `/users`;}
function getSingleUser(targetUserId) {return `/user/${targetUserId}`;}
function pollMessagesAfter(conversationId, lastMessageId) {return `/conversation/${conversationId}/new/${lastMessageId}`;}
function getLimitedMessages(conversationId, limit, offset) {return `/conversation/${conversationId}/message/limited?limit=${limit}&offset=${offset}`;}
function getLastSeen(conversationId, targetUserId) {return `/conversation/${conversationId}/lastseen/${targetUserId}`;}
function getConversation(conversationId) {return `/conversation/${conversationId}`;}
function getConversationsForUser() {return `/conversation/user/${userId}`;}
function sendMessage(conversationId) {return `/conversation/${conversationId}/message/send`;}
function createConversation() {return `/conversation/group`;}
function updateUserTimeStamp(conversationId) {return `/conversation/${conversationId}/seen/${userId}`;}

let chatAsset;
let messagesAsset;
let bubblesTemplate;


function setup() {
    $('#settingsModal').modal('show');
    $('#chatText').keydown(function(e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            __sendMessage();
        }
    });
    $.ajax({
        url: apiUrl + getAllUsers(),
        success: function(result) {
            result.forEach((user) => {
                let userDiv = document.createElement('div');
                userDiv.innerHTML = "<input name='usersSelection' type=checkbox value='" + user.id + "' class='checkbox checkbox-inline'/><span>" + user.name +"</span>";
                $('#userList').append(userDiv);
            });
        }
    })
}

function login() {
    // Normally this would have password as well but to simplify things let's leave it just auto logging in
    userId = $('#userField').val();
    loadChats();
}

function loadChats() {
    let ourChats;
    $.ajax({
        url: apiUrl + getConversationsForUser(),
        success: function(chatResult) {
            ourChats = chatResult;
            $.ajax({
                url: 'components/chatGroup.html',
                success: function(result) {
                    chatAsset = result;
                    // Reset our contacts to ensure it's empty
                    $('#myContacts').html();
                    let ourParser = new DOMParser();
                    let chatPromises = [];
                    for (let i=0; i < ourChats.length; i++) {
                        chatPromises.push(__getChatInfo(ourChats[i]));
                    }
                    Promise.all(chatPromises).then((results) => {
                        results.forEach((result) => {
                            let thisChatAsset = ourParser.parseFromString(chatAsset, 'text/html');
                            thisChatAsset.getElementById('contactName').innerHTML = result.title;
                            __setImage(thisChatAsset, result.title);
                            thisChatAsset.getElementById('messagePreviewText').innerHTML = result.message;
                            thisChatAsset.getElementById('chatAsset').setAttribute('onclick', 'selectChat(this, ' + result.chatId + ')');
                            $('#myContacts').append(thisChatAsset.getElementsByTagName('HTML')[0].innerHTML);
                            let contactTD = document.createElement('td');
                            contactTD.setAttribute('class', 'frequent-contact');
                            let frequentContact = document.createElement('div');
                            frequentContact.setAttribute('id', 'contactInitials');
                            frequentContact.setAttribute('style', 'display: inline');
                            frequentContact.setAttribute('class', 'contactInitial');
                            frequentContact.innerHTML = __getChatInitials(result.title);
                            contactTD.appendChild(frequentContact);
                            contactTD.setAttribute('onclick', 'selectChat(this, ' + result.chatId + ')');
                            $('#mobileContacts').append(contactTD);
                        });
                    });
                }
            });
        },
        error: function(error) {
            console.error(error);
        }
    });
}

function newChat() {
    const chatTitle = $('#chatName').val();
    let participants = [userId];
    let checkboxes = document.getElementsByName('usersSelection');
    checkboxes.forEach((checkbox) => {
        if (checkbox.checked) {
            participants.push(checkbox.value);
        }
    });
    let data = {'users': participants.toString(), 'name': chatTitle};
    $.ajax({
        url: apiUrl + createConversation(),
        method: 'POST',
        data: JSON.stringify(data),
        success: function(result) {
            loadChats();
            loadMessages(result.id);
        }
    })
}

function selectChat(asset, chatId) {
    // Clear any previous selections
    $('.table-contact').removeClass('activated');
    $(asset).addClass('activated');
    currentConversationId = chatId;
    loadMessages(chatId);
    __liveUpdates();
}

function loadMessages(targetChat) {
    const loadMessageViewTemplate = new Promise(function(resolve, reject) {
        $.ajax({
            url: 'components/messages.html',
            success: resolve,
            error: reject
        });
    });
    const loadMessageBubbleTemplate = new Promise(function(resolve, reject) {
        $.ajax({
            url: 'components/messageElement.html',
            success: resolve,
            error: reject
        });
    });



    Promise.all([loadMessageViewTemplate, loadMessageBubbleTemplate, __getMessages(targetChat), __getChatTitleFromId(targetChat)])
        .then(function(results) {
            messagesAsset = results[0];
            bubblesTemplate = results[1];
            let thisMessageHistory = results[2];
            $('#messageHeader').html(results[3][0]);
            $('#messageSubheader').html(results[3][1]);
            // Reset messages to ensure it's empty
            $('#myMessages').html(messagesAsset);
            for (let i=0; i < thisMessageHistory.length; i++) {
                insertNewMessage(thisMessageHistory[i], true);
            }
            currentMessageId = thisMessageHistory[thisMessageHistory.length - 1].id;
        }).then(() => {
            $('#messagesCanvas').scrollTop($('#messagesCanvas')[0].scrollHeight);
        });

}

function insertNewMessage(messageContent, isLoadingThread = false) {
    const canvas = $('#messagesCanvas');
    let ourParser = new DOMParser();
    let thisMessageBubble = ourParser.parseFromString(bubblesTemplate, 'text/html');
    if (messageContent.senderId === userId) {
        thisMessageBubble.getElementById('chatBubble').classList.add('sent');
        thisMessageBubble.getElementById('messageTime').classList.add('sent');
    } else {
        thisMessageBubble.getElementById('chatBubble').classList.add('received');
        thisMessageBubble.getElementById('messageTime').classList.add('received');
    }
    thisMessageBubble.getElementById('chatBubble').innerHTML = messageContent.message;
    thisMessageBubble.getElementById('messageTime').innerHTML = messageContent.timestamp;
    canvas.append(thisMessageBubble.getElementsByTagName('HTML')[0].innerHTML);

    if (!isLoadingThread) {
        if (!(canvas[0].scrollHeight - canvas.scrollTop() - canvas.height() - 50)) {
            // Not scrolled
            canvas.scrollTop(canvas[0].scrollHeight);
        }

        if (messageContent.senderId !== userId) {
            $('#newMessageTone')[0].play();
        }
    }
}

function __setImage(asset, chatTitle, chatImage = null) {
    if(chatImage){
        // We could add an image to make it look even better!
        // We have a contact image - display here
        let newImage = document.createElement('img');
        newImage.setAttribute('src', chatImage);
        // Select smaller measurement
        if (newImage.width > newImage.height) {
            const ratio = IMAGE_DIAMETER / newImage.height;
            newImage.setAttribute('height', IMAGE_DIAMETER + 'px');
            const leftPosition = ((newImage.width * ratio) - IMAGE_DIAMETER) / 2;
            newImage.setAttribute('style', 'margin-left: -' + leftPosition + 'px');
        } else {
            const ratio = IMAGE_DIAMETER / newImage.width;
            newImage.setAttribute('width', IMAGE_DIAMETER + 'px');
            const topPosition = ((newImage.height * ratio) - IMAGE_DIAMETER) / 2;
            newImage.setAttribute('style', 'margin-top: -' + topPosition + 'px');
        }
        asset.getElementById('contactImage').appendChild(newImage);
    } else {
        asset.getElementById('imageTextFallback').innerHTML = __getChatInitials(chatTitle);
    }
}

function __getChatInitials(chatTitle) {
    const initials = chatTitle ? chatTitle.split(' ') : '?';
    if (initials.length > 1) {
        return initials[0].charAt(0) + initials[initials.length - 1].charAt(0);
    }
    return initials[0].charAt(0);
}

function __getChatTitleFromId(chatId) {
    return new Promise(function(resolve) {
        $.ajax({
            url: apiUrl + getConversation(chatId),
            success: function(result) {
                return resolve(result);
            }
        })
    }).then((result) => {
        let promises = [__getChatTitle(result)];
        let members = result.users.length;
        if (members <= 2) {
            promises.push(__getLastSeenForUser(chatId, result.users.filter((user) => user.userid !== userId)[0].userid))
        } else {
            promises.push(new Promise(function(resolve) {
                return resolve("Chat with you and " + (members - 1) + " others");
            }))
        }
        return Promise.all(promises);
    })
}

function __getChatInfo(chat) {
    return Promise.all([__getChatTitle(chat), __getPreviewMessage(chat)]).then((results) => {
        return {'title': results[0], 'message': results[1], 'chatId': chat.conversation.id};
    })
}

function __getChatTitle(chat) {
    return new Promise(function(resolve) {
        if (chat.conversation.type === "1") {
            let chatUserId = chat.users.filter((user) => user.userid !== userId)[0];
            $.ajax({
                url: apiUrl + getSingleUser(chatUserId.userid),
                success: function(result) {
                    return resolve(result.name || "Unknown User");
                }
            });
        } else {
            return resolve(chat.conversation.name);
        }
    });
}

function __getPreviewMessage(chat) {
    // Return just the last message to display as a preview
    return new Promise(function(resolve) {
        $.ajax({
            url: apiUrl + getLimitedMessages(chat.conversation.conversationId, 1, 0),
            success: (result) => {
                return resolve(result.length ? result[0].message : "New Chat");
            }
        })
    })
}

function __getMessages(conversationId, limit = 20, offset = 0) {
    return new Promise(function(resolve) {
        $.ajax({
            url: apiUrl + getLimitedMessages(conversationId, limit, offset),
            success: (result) => {
                return resolve(result.reverse());
            }
        })
    })
}

function __getLastSeenForUser(conversationId, targetUserId) {
    return new Promise(function(resolve) {
        $.ajax({
            url: apiUrl + getLastSeen(conversationId, targetUserId),
            success: function(result) {
                return resolve("Last online: " + (result.lastseen || "Never"));
            }
        });
    });
}

function __liveUpdates() {
    // Poll every 500ms the status
    setTimeout(function() {
        $.ajax({
            url: apiUrl + updateUserTimeStamp(currentConversationId),
            method: 'PUT'
        });
        $.ajax({
            url: apiUrl + pollMessagesAfter(currentConversationId, currentMessageId),
            success: function(result) {
                if (result.length) {
                    result.forEach((function(message) {
                        insertNewMessage(message);
                        currentMessageId = message.id;
                    }));
                }
                __liveUpdates();
            },
            error: function(error) {
                // Could be the result of no messages
                __liveUpdates();
            }
        });
    }, 500);
}

function __sendMessage() {
    const newMessageContent = {'message': $('#chatText').val(), 'senderId': userId, 'timestamp': 'Sent'};
    insertNewMessage(newMessageContent);
    $.ajax({
        url: apiUrl + sendMessage(currentConversationId),
        method: 'POST',
        data: JSON.stringify(newMessageContent),
        success: function(result) {
            currentMessageId = result.id;
        }
    });
    $('#chatText').val('');
}